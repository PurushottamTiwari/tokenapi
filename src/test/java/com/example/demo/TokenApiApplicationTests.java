package com.example.demo;

import com.example.service.TokenService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


@SpringBootTest

class TokenApiApplicationTests {

	static String TOKEN = null;

	@Autowired
	TokenService tokenService;

	static final String ISSUE = "Purushottam-HpLaptop";
	static final String SUB = "jwttoken";
	static final String REF_TYPE = "entityid";
	static final String REF = "562648584";
	static final String KEY = "Puru#2012";

	@Test
	void a() {

		Calendar calendar = Calendar.getInstance();

		JwtToken jwtToken = new JwtToken();
		jwtToken.setIssueer(ISSUE);
		jwtToken.setSubject(SUB);

		Map<String , Object> claims = new HashMap<>();
		claims.put(TokenConstant.HEADER_PARAM_REF_TYPE,REF_TYPE);
		claims.put(TokenConstant.HEADER_PARAM_REF_VALUE,REF);
		claims.put(TokenConstant.HEADER_PARAM_SEC_VAL,KEY);

        String token = tokenService.createToken(jwtToken,claims,calendar.getTime());
		TOKEN = token;
        assert(StringUtils.isEmpty(token) == false);
	}

	@Test
	void b() {
        String key = "Puru#2012";

		TokenDetails tokenDetails = new TokenDetails();
		tokenDetails.setIssuer(ISSUE);
		tokenDetails.setSubject(SUB);
		tokenDetails.setServiceIdentifierReferenceType(REF_TYPE);
		tokenDetails.setServiceIdentifierReference(REF);

		Calendar calendar = Calendar.getInstance();

		boolean result = tokenService.verifyToken(TOKEN,KEY,tokenDetails);


		assert(result ==  true);
	}

}
