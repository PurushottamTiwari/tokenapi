package com.example.demo;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TokenMVCMock {

    static String token = null;
    @Autowired
    WebApplicationContext webApplicationContext;

    MockMvc  mockMvc;

    @Before
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();

    }

    @Test
    @Order(value = 1)
    public void a(){
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("serviceidentifierreference", "526145847");
            httpHeaders.add("serviceidentifierreferencetype", "entitynumber");
            httpHeaders.add("securitykey", "Puru#2012");
            httpHeaders.add("accept" ,"application/json");
            httpHeaders.add("Content-Type","application/json");


           JwtToken jwtToken = new JwtToken();
           jwtToken.setIssueer("Purushottam");
           jwtToken.setSubject("TokenAPI");

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            String requestJson=ow.writeValueAsString(jwtToken );





            RequestBuilder requestBuilder = MockMvcRequestBuilders.request(HttpMethod.POST, "http://localhost:8082/token").headers(httpHeaders).content(requestJson);
            MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

            System.out.println("Before :"+ mvcResult.getResponse().getContentAsString());

            httpHeaders.add("If-Modifies-Since","2020-05-13T12:01:40.285+0000");
            requestBuilder = MockMvcRequestBuilders.request(HttpMethod.POST, "http://localhost:8082/token").headers(httpHeaders).content(requestJson);
            mvcResult = mockMvc.perform(requestBuilder).andReturn();
            System.out.println("After :"+ mvcResult.getResponse().getContentAsString());


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*@Test
    @Order(value = 2)
    public void b(){
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Authorization", token);
            httpHeaders.add("key", "Puru#2012");
            httpHeaders.add("accept" ,"application/xml");
            httpHeaders.add("Content-Type","application/xml");
            RequestBuilder requestBuilder = MockMvcRequestBuilders.request(HttpMethod.POST, "http://localhost:8082/verify").headers(httpHeaders).contentType("application/jsopn");
            MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
            System.out.println(mvcResult.getResponse().getContentAsString());
        }catch (Exception e){
            e.printStackTrace();
        }

    }*/



}
