package com.example.service;

import com.example.demo.JwtToken;
import com.example.demo.TokenDetails;

import java.util.Date;
import java.util.Map;

public interface TokenService {
    String createToken(JwtToken jwtToken, Map<String, Object> claims, Date createdAt);
    Boolean verifyToken(String token, String key, TokenDetails tokenDetails);
}
