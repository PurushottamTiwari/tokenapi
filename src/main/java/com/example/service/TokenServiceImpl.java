package com.example.service;

import com.example.demo.JwtToken;
import com.example.demo.TimeHelper;
import com.example.demo.TokenConstant;
import com.example.demo.TokenDetails;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

@Service
public class TokenServiceImpl implements TokenService{
    @Override
    public String createToken(JwtToken jwtToken, Map<String, Object> claims, Date createdAt) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(createdAt);
        calendar.add(Calendar.SECOND,300);
        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setSubject(jwtToken.getSubject());
        jwtBuilder.setIssuer(jwtToken.getIssueer());
        System.out.println(createdAt);
        jwtBuilder.setIssuedAt(createdAt);
        System.out.println(calendar.getTime());
        jwtBuilder.setExpiration(calendar.getTime());
        jwtBuilder.addClaims(claims);
        byte[] apiKeySecretBytes =  DatatypeConverter.parseBase64Binary((String) claims.get(TokenConstant.HEADER_PARAM_SEC_VAL));
        jwtBuilder.signWith(SignatureAlgorithm.HS256,apiKeySecretBytes);

        String token = jwtBuilder.compact();


        return token;
    }
    public Boolean verifyToken(String token, String key, TokenDetails tokenDetails){

       try {
           byte[] apiKeySecretBytes =  DatatypeConverter.parseBase64Binary(key);
           Jws<Claims> jws  = Jwts.parser().setSigningKey(apiKeySecretBytes).parseClaimsJws(token);
           Claims claims = jws.getBody();
           System.out.println("JWT decoded: " + jws);
           System.out.println("Subject: " + claims.getSubject());
           System.out.println("Issuer: " + claims.getIssuer());
           System.out.println("Issued at: " + claims.getIssuedAt());
           System.out.println("Expiration: " + claims.getExpiration());
           System.out.println("Some_Id: " + claims.get("some_id"));

           if(!claims.get(TokenConstant.HEADER_PARAM_REF_TYPE).equals(tokenDetails.getServiceIdentifierReferenceType()))return false;
           if(!claims.get(TokenConstant.HEADER_PARAM_REF_VALUE).equals(tokenDetails.getServiceIdentifierReference()))return false;
         

       }catch (ExpiredJwtException e){
           throw e;
       }
       return true;
    }
}
