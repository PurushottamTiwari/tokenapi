package com.example.demo;

import java.security.Key;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TestJWTToken {
    private static final String API_KEY = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static void main(String... args) {
        String jwt = createJWT();
        System.out.println("JWT: " + jwt);
        parseJWT(jwt);
    }

    private static String createJWT() {
        Calendar cal = Calendar.getInstance(Locale.UK);
        Calendar cal1 = Calendar.getInstance(Locale.UK);
        cal1.setTime(cal.getTime());
        cal1.add(Calendar.SECOND, 1);

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(API_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, SignatureAlgorithm.HS256.getJcaName());

        Map<String, Object> map = new HashMap<>();
        map.put("alg", "HS256");
        map.put("typ", "JWT");

        String someId = UUID.randomUUID().toString();

        return Jwts.builder().setHeader(map).setIssuer("service_provider").setSubject("consumer_provider_connectivity_token")
                .claim("some_id", someId).setIssuedAt(cal.getTime()).setExpiration(cal1.getTime())
                .signWith(SignatureAlgorithm.HS256, signingKey).compact();
    }

    private static void parseJWT(String jwt) {
        Jws<Claims> jwsClaims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(API_KEY)).parseClaimsJws(jwt);
        System.out.println("JWT decoded: " + jwsClaims);

        Claims claims = jwsClaims.getBody();
        System.out.println("Subject: " + claims.getSubject());
        System.out.println("Issuer: " + claims.getIssuer());
        System.out.println("Issued at: " + claims.getIssuedAt());
        System.out.println("Expiration: " + claims.getExpiration());
        System.out.println("Some_Id: " + claims.get("some_id"));
    }
}