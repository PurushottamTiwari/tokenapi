package com.example.demo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

@Getter
@Setter
public class JwtToken {

    private String issueer;
    private String subject;

    public String getIssueer() {
        return issueer;
    }

    public void setIssueer(String issueer) {
        this.issueer = issueer;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
