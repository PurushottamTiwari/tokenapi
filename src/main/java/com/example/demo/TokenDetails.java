package com.example.demo;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;
@Getter
@Setter
public class TokenDetails {
    private String issuer;
    private String subject;
    private Date issuedAt;
    private String serviceIdentifierReferenceType;
    private String serviceIdentifierReference;

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Date issuedAt) {
        this.issuedAt = issuedAt;
    }

    public String getServiceIdentifierReferenceType() {
        return serviceIdentifierReferenceType;
    }

    public void setServiceIdentifierReferenceType(String serviceIdentifierReferenceType) {
        this.serviceIdentifierReferenceType = serviceIdentifierReferenceType;
    }

    public String getServiceIdentifierReference() {
        return serviceIdentifierReference;
    }

    public void setServiceIdentifierReference(String serviceIdentifierReference) {
        this.serviceIdentifierReference = serviceIdentifierReference;
    }
}
