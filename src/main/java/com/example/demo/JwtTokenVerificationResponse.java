package com.example.demo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtTokenVerificationResponse {
    private String responseCode;
    private String responseStr;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }
}
