package com.example.demo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeHelper {

    public static Date addOneMinuteToDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE,1);
        return calendar.getTime();
    }
    public static Date getDate(String date) throws  Exception{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY/MM/dd hh:mm:ss");
        return simpleDateFormat.parse(date);

    }

}
