package com.example.demo;

import com.example.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.*;

@RestController
public class TokenController {

	@Autowired
	TokenService tokenService;



	@RequestMapping(path = "/token", method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})

    @ResponseBody
	public JwtTokenResponse getToken(@RequestBody JwtToken jwtToken, @RequestHeader("serviceidentifierreference") String serviceidentifierreference,
                                     @RequestHeader("serviceidentifierreferencetype") String serviceidentifierreferencetype, @RequestHeader("securitykey")String key) {
		JwtTokenResponse jwtTokenResponse = new JwtTokenResponse();
		try {
               Calendar calendar = Calendar.getInstance();
               Map<String , Object> claims = new HashMap<>();
               claims.put(TokenConstant.HEADER_PARAM_REF_TYPE,serviceidentifierreferencetype);
               claims.put(TokenConstant.HEADER_PARAM_REF_VALUE,serviceidentifierreference);
               claims.put(TokenConstant.HEADER_PARAM_SEC_VAL,key);
               String token  = tokenService.createToken(jwtToken,claims, calendar.getTime());

               TokenDetails tokenDetails =  new TokenDetails();
               tokenDetails.setIssuer(jwtToken.getIssueer());
               tokenDetails.setSubject(jwtToken.getSubject());
               tokenDetails.setServiceIdentifierReference(serviceidentifierreference);
               tokenDetails.setServiceIdentifierReferenceType(serviceidentifierreferencetype);

               TokenConstant.tokenMap.put(key,tokenDetails);


            jwtTokenResponse.setToken(TokenConstant.TEXT);
            jwtTokenResponse.setToken_Value(token);
            jwtTokenResponse.setLastGenerated(new Date());


		}catch(Exception e){
			e.printStackTrace();
		}
		return jwtTokenResponse;
	}

	@RequestMapping(path ="/verify",method=RequestMethod.POST)
    @ResponseBody
	public JwtTokenVerificationResponse  verifyToken(@RequestHeader("Authorization") String authorization,@RequestHeader("key")String key){
        boolean res = tokenService.verifyToken(authorization,key,TokenConstant.tokenMap.get(key));
        JwtTokenVerificationResponse jwtTokenVerificationResponse = new JwtTokenVerificationResponse();
        if(res){

            jwtTokenVerificationResponse.setResponseCode("200");
            jwtTokenVerificationResponse.setResponseStr("Access Granted");
        }
        return jwtTokenVerificationResponse;
	}

}
