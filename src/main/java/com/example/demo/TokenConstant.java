package com.example.demo;

import java.util.HashMap;
import java.util.Map;

public class TokenConstant {

    public  static final  String HEADER_PARAM_REF_TYPE = "SERVICE_IDENTIFIER_REFERENCE_TYPE";
    public  static final  String HEADER_PARAM_REF_VALUE = "SERVICE_IDENTIFIER_REFERENCE";
    public  static final  String HEADER_PARAM_SEC_VAL = "SECURITY_KEY";
    public  static final  String TEXT = "servicetoken";

    public static Map<String, TokenDetails> tokenMap =  new HashMap<>();
}
